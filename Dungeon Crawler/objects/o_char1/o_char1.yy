{
    "id": "097831af-4344-4621-b260-791bde3cf83f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_char1",
    "eventList": [
        {
            "id": "fd5219c6-f3b1-4438-a63e-5f06169a81e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "99879e7f-1928-41b9-9a8c-e9476c561a16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "7857d09a-3a83-49c1-8d68-63ee72b812a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "71abb6aa-31ab-4aa6-9995-45066dbab973",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "d00b0a87-097f-4f9e-9dba-dc7ca88cd96b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 10,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "78ef8b69-7845-4818-9d2b-720fa96962a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 10,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "2af2c40a-fa06-4eb8-94c2-8228fe365d9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 10,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "68459f68-9d07-4af2-9b20-df67cdd780cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 10,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "991adbbf-a028-45c9-b28e-2019144d37b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "9148528e-9be5-42af-b0b4-62bb84095e0a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        },
        {
            "id": "89368110-b388-4960-bb4f-684647cfd449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e0e2ab20-d6c8-4cd3-93f4-67d0b365d50e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "097831af-4344-4621-b260-791bde3cf83f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "541dee59-9c52-46be-958e-8af3b86d39c3",
    "visible": true
}