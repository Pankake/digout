{
    "id": "5cc11faf-97ff-475e-a6ff-ef69ccadabc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cll_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4541374-a612-46ba-9cca-eda8781d192e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc11faf-97ff-475e-a6ff-ef69ccadabc2",
            "compositeImage": {
                "id": "e71afe21-f52a-442c-ac92-56f20004f003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4541374-a612-46ba-9cca-eda8781d192e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545db795-af8d-4337-9635-83c0d931b90f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4541374-a612-46ba-9cca-eda8781d192e",
                    "LayerId": "c237f68d-9003-4baa-9b58-3e4804888232"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c237f68d-9003-4baa-9b58-3e4804888232",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cc11faf-97ff-475e-a6ff-ef69ccadabc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}