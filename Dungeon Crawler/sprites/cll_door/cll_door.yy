{
    "id": "690c197c-2e46-4fd9-8429-3eb1fb146996",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cll_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "166f9e7d-6f4b-49f3-9ea5-f25785b7f8a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "690c197c-2e46-4fd9-8429-3eb1fb146996",
            "compositeImage": {
                "id": "e0af5bf4-6765-4d81-ac8e-cd525d82fbcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "166f9e7d-6f4b-49f3-9ea5-f25785b7f8a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3b1447-82d6-4a58-b869-6fea9ee5ab63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "166f9e7d-6f4b-49f3-9ea5-f25785b7f8a7",
                    "LayerId": "e16f7902-483b-4c69-b35d-5720da530bb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e16f7902-483b-4c69-b35d-5720da530bb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "690c197c-2e46-4fd9-8429-3eb1fb146996",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}