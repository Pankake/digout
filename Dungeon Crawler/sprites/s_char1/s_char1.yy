{
    "id": "541dee59-9c52-46be-958e-8af3b86d39c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_char1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a32232b-12d0-4fdd-90ce-aa1bde52e136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "541dee59-9c52-46be-958e-8af3b86d39c3",
            "compositeImage": {
                "id": "8da70523-915c-46db-8adc-c341a902e62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a32232b-12d0-4fdd-90ce-aa1bde52e136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4daba13-23ce-4aca-ac43-e27e14c31a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a32232b-12d0-4fdd-90ce-aa1bde52e136",
                    "LayerId": "a2981732-ec97-414b-9639-09636acb5457"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a2981732-ec97-414b-9639-09636acb5457",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "541dee59-9c52-46be-958e-8af3b86d39c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}