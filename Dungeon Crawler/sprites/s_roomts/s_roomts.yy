{
    "id": "8ff70999-3a53-496e-92dd-be04745b3538",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_roomts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef0ac39e-2bde-4554-bf94-35d7bd883797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff70999-3a53-496e-92dd-be04745b3538",
            "compositeImage": {
                "id": "f790e2f0-dcfa-4134-a781-546dfb21b4d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0ac39e-2bde-4554-bf94-35d7bd883797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db3168da-d591-4041-a44d-9a0187851bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0ac39e-2bde-4554-bf94-35d7bd883797",
                    "LayerId": "1ffe6c11-97df-4c99-8788-821408aabe11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1ffe6c11-97df-4c99-8788-821408aabe11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ff70999-3a53-496e-92dd-be04745b3538",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}